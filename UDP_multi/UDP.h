#pragma once
#ifndef __UDP_H
#define __UDP_H

#define MAX_BUFSIZE  1300

//コマンド
#define	    INIT_CMD				"IRC" //初期化コマンド
#define	    MONITOR_CMD				"MRC" //モニターコマンド

#define     MESU_WRITE_CMD          "MEW" //計測値の書き換えコマンド
#define	    MODE_SET_CMD            "MDS" //モードセットコマンド
#define	    SOFT_WEAR_UPDATE_CMD    "SWU" //ソフトウェアアップデートコマンド
#define	    SOFT_WEAR_CHANGE_CMD    "SWC" //ソフトウェア・チェンジコマンド
#define     IWDT_RESET_TEST_CMD     "IWD" //IWDT RESET テストコマンド
#define     MEMORY_READ_CMD         "MRD" //メモリーリードコマンド
#define     MEMORY_WRITE_CMD        "MWR" //メモリーライトコマンド
#define     GET_RESET_VECTOR_CMD    "GRV" //RESET VECOTR 取得コマンド
#define     GET_VERSION_CMD         "GVR" //VERSION 取得コマンド
#define     GET_DISP_INITIAL        "GDI" //電源ON初期化時のメッセージ取得
#define     SET_DATAFLASH_CMD       "SDF" //データフラッシュ・テーブル設定コマンド
#define     GET_DATAFLASH_CMD       "GDF" //データフラッシュ・テーブル取得コマンド
#define     READ_DATAFLASH_CMD      "RDF" //データフラッシュ・テーブルREADコマンド
#define     WRITE_DATAFLASH_CMD     "WDF" //データフラッシュ・テーブルWRITEコマンド
//パラメータ
#define     MODE_SET_PARAM_NORMAL           "PNR" //モード・通常
#define     MODE_SET_PARAM_SOFT_WEAR_UPDATE "PSU" //モード・セット・パラメータ・ソフトアップデート
#define     MODE_SET_PARAM_MEASURE_UPDATE   "PMU" //モード・セット・パラメータ・計測値データデート

#define     RETURN_PARAM_OK         "OK"
#define     RETURN_PARAM_NG         "NG"


void startUDP(char *dest_ip, int dest_port);
void kanpUDP(ARGV_STRUCT_st argV);
int udp_receive(SOCKET sock, char* buffer, int size, struct sockaddr* sockaddr);
void socket_close(int server);
void receiveUDP(SOCKET *sock);
void dispMemDump(char* adr, uint32_t size);
void helpKanpUDP(void);
int cmdMonitor(int param);
int cmdInit(void);
int cmdGetVersion(void);

#endif //__UDP_H