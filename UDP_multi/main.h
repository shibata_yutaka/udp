#pragma once
#ifndef __MAIN_H
#define __MAIN_H

#define APL_VERSION 1.0
#define PORT 8501 //192.168.1.140

enum
{
	FILE_OPEN = 0,
	FILE_WRITE,
	FILE_CLOSE,
	FILE_STATE_MAX
};

typedef struct
{
	char strS[256];
	char* strP;
	char* ctx;
} ARGV_STRUCT_st;


void help(void);


extern ARGV_STRUCT_st argV;

#endif //__MAIN_H
