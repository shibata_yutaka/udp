
/*
*  参考
*  http://y-okamoto-psy1949.la.coocan.jp/VCpp/ThreadVCpp2013/
* 
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <time.h>
//#include <sys/types.h>

#include <winsock2.h>
#include <iostream>
#include <vector>
#include <thread>
#include <functional>
#include <atomic>

#include "main.h"
#include "UDP.h"
/*
*
*/
extern const char bms_ip[4][16];
extern int bms_port;
extern SOCKET sockUdpFD[]; // PLC socket
extern int currentNo;

extern void kanpUDP(ARGV_STRUCT_st argV);
extern void startUDP(char* dist_ip, int dist_port);
extern int cmdMonitor(int param);
extern int cmdInit(void);
extern int cmdGetVersion(void);

void sendUDPthread(void);
void receiveUDPthread(void);

void sendUDPthread(void)
{

    int param = 0;

    printf("%s %d start\n", __func__, __LINE__);



    //初期通信(バージョン)取得
    cmdInit();

    while (1)
    {
        //usleep();
        Sleep(1000);   //#include <winsock2.h>

        //モニタ受信
        cmdMonitor(param);
        
    }

}
/*
* UDP受信スレッド
*/
void receiveUDPthread(void)
{
    printf("%s %d start\n", __func__, __LINE__);
    while (1)
    {
        receiveUDP(&sockUdpFD[currentNo]);
    }
}

/*
* スレッド開始
*/
int startTimerThread(void)
{
    printf("%s %d start\n", __func__, __LINE__);

    startUDP((char*)&bms_ip[0], bms_port);


    std::vector<std::thread> threads;


    //UDP送信スレッド
    threads.push_back(std::thread{ sendUDPthread });



    //UDP受信スレッド
    threads.push_back(std::thread{ receiveUDPthread });




    std::vector<std::thread>::iterator t;

    for (t = threads.begin(); t != threads.end(); t++)
    {
        t->join();
    }

    return 0;
}