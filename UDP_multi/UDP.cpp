#include <stdio.h>
#include <iostream>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <WinSock2.h> //inet_ntp()

#include "main.h"
#include "UDP.h"

SOCKET sockUdpFD[4]; // PLC socket
struct sockaddr receiveSockAddr;


const char kanp_ip[] = "192.168.1.140";
const char bms_ip[4][16]  = 
{
    "192.168.1.120",
    "192.168.1.121",
    "192.168.1.122",
    "192.168.1.123"
};

int bms_port  = 8510;
int kanp_port = 8520;

char recvBuffer[MAX_BUFSIZE];
int  recvLen = 0;

int currentNo=0;

extern int startTimerThread(void);

/*
* 監視PCもどき
*/
void kanpUDP(ARGV_STRUCT_st argV)
{
    printf("%s %d\n", __func__, __LINE__);

    char* strp = argV.strP;
    char* ctx  = argV.ctx;

    int size=0;
    int ret = 0;

    char data[256] = "";
    memset(data, NULL, sizeof(data));


    printf("%s %d strS=%s strP=%s\n", __func__, __LINE__, argV.strS,argV.strP);
    strp = strtok_s(NULL, " \n\r", &ctx);

    if (strp == NULL)
    {
        printf("%s %d\n", __func__, __LINE__);
        //スレッドで監視PCから1秒ごとに取得コマンドを送信する
        startTimerThread();
    }
    else
    {
        printf("%s %d strS=%s strP=%s\n", __func__, __LINE__, argV.strS, argV.strP);
        // data フォルダが無い場合はフォルダを作成する /////////
        char cmd[100];
        sprintf_s(cmd, "if not exist data mkdir data");
        system(cmd);

        //通信初期化
        startUDP((char*)bms_ip, bms_port);

        if (strcmp(strp, INIT_CMD) == 0)
        {
            //初期通信要求
            cmdInit();
        }
        else if (strcmp(strp, MONITOR_CMD) == 0)
        {
            //モニタ要求
            int param = 0;
            strp = strtok_s(NULL, " \n\r", &ctx);
            if (strp == NULL)
            {
                param = 0;
            }
            else
            {
                // パラメータを数値に変換
                param = atoi(strp);
            }
            cmdMonitor(param);
        }
        else if (strcmp(strp, GET_VERSION_CMD) == 0)
        {
            //バージョン要求
            cmdGetVersion();
        }
        else if (strcmp(strp, "help") == 0)
        {
            size = 0;
            helpKanpUDP();
        }
        else
        {
            size = 0;
            helpKanpUDP();
        }

        if (size > 0)
        {

            //startUDP((char*)bms_ip);
            //printf("%s %d %s\n\r", __func__, __LINE__, data);
            //RackBMSへコマンド送信
            //send(sockUdpFD[currentNo], (char*)&data, (int)strlen(data), 0);
            
            receiveUDP(&sockUdpFD[currentNo]);
        }

    }
    
}
/*
* モニターコマンド送信
*/
int cmdMonitor(int param)
{
    int size = 0;
    int ret = 0;
    char data[256] = "";


    // MRC モニタコマンド : CMD(3),PARAM(3)
    memset(data, 0x00, sizeof(data));
    sprintf_s(data, "%s%02d%03d", MONITOR_CMD, 0, param);
    size = (int)strlen(data);

    memset(data, NULL, sizeof(data));
    // IRC 初期化 : CMD(3)
    memset(data, 0x00, sizeof(data));
    memcpy(data, MONITOR_CMD, strlen(MONITOR_CMD));
    size = (int)strlen(data);
    printf("%s %d size=%d data=%s\n\r", __func__, __LINE__, size, data);

    //RackBMSへコマンド送信
    send(sockUdpFD[currentNo], (char*)&data, (int)strlen(data), 0);

    return ret;
}
/*
* 初期通信コマンド送信
*/
int cmdInit(void)
{
    int size = 0;
    int ret = 0;

    char data[256] = "";

    // IRC 初期化 : CMD(3)
    memset(data, 0x00, sizeof(data));
    memcpy(data, INIT_CMD, strlen(INIT_CMD));
    size = (int)strlen(data);
    printf("%s %d size=%d data=%s\n\r", __func__, __LINE__, size, data);


    //RackBMSへコマンド送信
    send(sockUdpFD[currentNo], (char*)&data, (int)strlen(data), 0);

    return ret;
}

/*
* バージョン取得コマンド
*/
int cmdGetVersion(void)
{
    int size = 0;
    int ret = 0;

    char data[256] = "";

    // GVR バージョンの取得 : CMD(3)
    memset(data, 0x00, sizeof(data));
    memcpy(data, GET_VERSION_CMD, strlen(GET_VERSION_CMD));
    size = (int)strlen(data);
    printf("%s %d size=%d data=%s\n\r", __func__, __LINE__, size, data);


    //RackBMSへコマンド送信
    send(sockUdpFD[currentNo], (char*)&data, (int)strlen(data), 0);

    return ret;
}

/*
* ヘルプ表示
*/
void helpKanpUDP(void)
{
    printf("kanp\n");
    printf("GVR : Get Version\n");
    printf("IRC : INIT\n");
    printf("MRC <param> : Monitor <param=0,1,2...>\n");
}

/*
*  UDP通信開始処理
* 引数(1) char *dist_ip  : 送信先のIP アドレス
* 引数(2) int  dist_port : 送信先のポート
*/
void startUDP(char *dist_ip, int dist_port)
{
    printf("%s %d IP=%s Port=%d\n", __func__, __LINE__,dist_ip, dist_port);

    struct sockaddr_in bmsAddr;

    // ソケット生成
    if ((sockUdpFD[currentNo] = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
    {
        printf("%s %d client socket make error\n", __func__, __LINE__);
        perror("socket");
    }
    else
    {
        printf("%s %d sockUdpFD[%d]=%d\n",__func__,__LINE__,currentNo, (int)sockUdpFD[currentNo]);
    }


    // 送信先アドレス・ポート番号設定
    // *dist_ip  : 送信先のIP アドレス
    // dist_port : 送信先のポート
    bmsAddr.sin_family = AF_INET;
    inet_pton(AF_INET, dist_ip, &bmsAddr.sin_addr.s_addr);
    bmsAddr.sin_port = htons(dist_port);

    printf("%s %d Destination IP=%d.%d.%d.%d port=%d\n", __func__, __LINE__, (bmsAddr.sin_addr.s_addr & 0xff),
        ((bmsAddr.sin_addr.s_addr & 0xff00) >> 8),
        ((bmsAddr.sin_addr.s_addr & 0xff0000) >> 16),
        ((bmsAddr.sin_addr.s_addr & 0xff000000) >> 24),
        ntohs(bmsAddr.sin_port)
    );


    if (connect(sockUdpFD[currentNo], (struct sockaddr*)&bmsAddr, sizeof(bmsAddr)) < 0)
    {
        printf("%s %d sockUdpFD[%d]=%d\n", __func__, __LINE__, currentNo,(int)sockUdpFD[currentNo]);
    }
    else
    {
        printf("%s %d sockUdpFD[%d]=%d\n", __func__, __LINE__, currentNo,(int)sockUdpFD[currentNo]);
    }
}

/*
* UDP受信処理
*/
void receiveUDP(SOCKET *sock)
{
    printf("%s %d sock=%d\n", __func__, __LINE__,(int)sockUdpFD[currentNo]);
    memset(&recvBuffer, 0x00, sizeof(recvBuffer));
    while (1)
    {
        int size = udp_receive(sockUdpFD[currentNo], &recvBuffer[0], MAX_BUFSIZE, &receiveSockAddr);
        if (size < 0) {
            perror("recvfrom() failed.");
            //return MSG_FAILURE;
            return;
        }
        else
        {
            // メモリーダンプ
            dispMemDump(&recvBuffer[0], size);
            return;
        }
    }
}


/*
*  UDP 受信処理
*/
int udp_receive(SOCKET sock, char* buffer, int size, struct sockaddr* sockaddr)
{
    int sockaddrLen = sizeof(*sockaddr);
    int receivedSize = recvfrom(sock, buffer, MAX_BUFSIZE, 0, sockaddr, &sockaddrLen);
    if (receivedSize < 0) {
        perror("recvfrom() failed.");
        //return MSG_FAILURE;
        return 0;
    }

    //<Winsock2.h>
    int port = ntohs(((struct sockaddr_in*)sockaddr)->sin_port);    
    char str[INET_ADDRSTRLEN];
    inet_ntop(((struct sockaddr_in*)sockaddr)->sin_family, &((struct sockaddr_in*)sockaddr)->sin_addr,str,sizeof(str));
    printf("%s %d Receive IP=%s Port=%d\n",__func__,__LINE__,str,port);
    
    return receivedSize;
}
/*
* ソケットのクローズ
*/
void socket_close(int server)
{
    if (closesocket(server) < 0) {
        perror("close() failed.");
        //exit(EXIT_FAILURE);
    }
}

/*
* メモリーダンプ
*/
void dispMemDump(char *adr, uint32_t size)
{
    printf("%s %d size=%d\n", __func__, __LINE__, size);
    //for (int i = 0; i < (int)size; i++)
    //{
        //printf("%02X ",(*(adr+i)&0xFF));
    //}
    //printf("\n");


    printf("     ");
    for (int j = 0; j < 16; j++)
    {
        printf("%02X ", j);
    }
    printf("\n");
    for (int j = 0; j < (int)size; j++)
    {
        if ((j % 16) == 0)
        {
            printf("%04X ", j);
        }
        printf("%02X ", ( *(adr+j) & 0xFF));
        if ((j % 16) == 15)
        {
            printf("\n");
        }
    }
    printf("\n");
}


