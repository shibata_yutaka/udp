
/*
*  参考
*  http://y-okamoto-psy1949.la.coocan.jp/VCpp/ThreadVCpp2013/
* 
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <time.h>
//#include <sys/types.h>

#include <winsock2.h>
#include <iostream>
#include <vector>
#include <thread>
#include <functional>
#include <atomic>

#include "main.h"
#include "UDP.h"
/*
*
*/

unsigned char recvBuffer[MAX_BUFSIZE];

extern char bms_ip[];
extern int bms_port;
extern SOCKET sockUdpFD;
extern int currentNo;

extern void kanpUDP(ARGV_STRUCT_st argV);
extern void startUDP(char* dist_ip, int dist_port);
extern int cmdMonitor(int param);
extern int cmdInit(void);
extern int cmdGetVersion(void);

extern int receiveUDP(SOCKET* sock, char* rBuff);
extern void dispMemDump(char* adr, uint32_t size);
extern void file_write(char* str);
extern int analyzeReciveCmd(char* rBuff, char* str, int size);

void sendUDPthread(void);
void receiveUDPthread(void);

void sendUDPthread(void)
{

    int param = 0;

    printf("%s %d start\n", __func__, __LINE__);

    //初期通信(バージョン)取得
    cmdInit();

    while (1)
    {
        //usleep();
        Sleep(1000);   //#include <winsock2.h>

        //モニタ受信
        cmdMonitor(param);
        
    }

}
/*
* UDP受信スレッド
*/
void receiveUDPthread(void)
{
    printf("%s %d start\n", __func__, __LINE__);
    //char rBuff[MAX_BUFSIZE];
    char str[2048];
    while (1)
    {
        int size = receiveUDP(&sockUdpFD, (char*)&recvBuffer);
        //受信したデータをファイル保存する
        if (size >= 0)
        {
            //受信データをメモリーダンプする
            //dispMemDump((char*)&recvBuffer, size);

            memset(str, 0x00, sizeof(str));
            //受信したデータを解析する
            int ret = analyzeReciveCmd((char*)&recvBuffer, str, sizeof(str));

            //ファイルに書き出す
            if (ret > 0)
            {
                file_write(&str[0]);
            }
            
        }
        //
    }
}

/*
* スレッド開始
*/
int startTimerThread(void)
{
    printf("%s %d start\n", __func__, __LINE__);

    // UDP通信開始
    startUDP((char*)bms_ip, bms_port);


    std::vector<std::thread> threads;


    //UDP送信スレッド
    threads.push_back(std::thread{ sendUDPthread });



    //UDP受信スレッド
    threads.push_back(std::thread{ receiveUDPthread });




    std::vector<std::thread>::iterator t;

    for (t = threads.begin(); t != threads.end(); t++)
    {
        t->join();
    }

    return 0;
}