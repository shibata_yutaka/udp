#pragma once
#ifndef __UDP_H
#define __UDP_H


#define MONITORING_PC_COMM_SEND_CMD_LENGTH  11 //監視PCとの通信コマンドサイズ
#define MONITORING_PC_COMM_CHECKSUM_LENGTH   1 //監視PCとの通信モニタコマンドのチェックサムサイズ
#define MAX_BUFSIZE  1300
#define MONITORING_PC_MONITOR_DATA_NUM     374 //監視PCとの通信モニタコマンドデータサイズ  

//コマンド
#define	    INIT_CMD				"IRC" //初期化コマンド
#define	    MONITOR_CMD				"MRC" //モニターコマンド

#define     MESU_WRITE_CMD          "MEW" //計測値の書き換えコマンド
#define	    MODE_SET_CMD            "MDS" //モードセットコマンド
#define	    SOFT_WEAR_UPDATE_CMD    "SWU" //ソフトウェアアップデートコマンド
#define	    SOFT_WEAR_CHANGE_CMD    "SWC" //ソフトウェア・チェンジコマンド
#define     IWDT_RESET_TEST_CMD     "IWD" //IWDT RESET テストコマンド
#define     MEMORY_READ_CMD         "MRD" //メモリーリードコマンド
#define     MEMORY_WRITE_CMD        "MWR" //メモリーライトコマンド
#define     GET_RESET_VECTOR_CMD    "GRV" //RESET VECOTR 取得コマンド
#define     GET_VERSION_CMD         "GVR" //VERSION 取得コマンド
#define     GET_DISP_INITIAL        "GDI" //電源ON初期化時のメッセージ取得
#define     SET_DATAFLASH_CMD       "SDF" //データフラッシュ・テーブル設定コマンド
#define     GET_DATAFLASH_CMD       "GDF" //データフラッシュ・テーブル取得コマンド
#define     READ_DATAFLASH_CMD      "RDF" //データフラッシュ・テーブルREADコマンド
#define     WRITE_DATAFLASH_CMD     "WDF" //データフラッシュ・テーブルWRITEコマンド
//パラメータ
#define     MODE_SET_PARAM_NORMAL           "PNR" //モード・通常
#define     MODE_SET_PARAM_SOFT_WEAR_UPDATE "PSU" //モード・セット・パラメータ・ソフトアップデート
#define     MODE_SET_PARAM_MEASURE_UPDATE   "PMU" //モード・セット・パラメータ・計測値データデート

//モニタコマンドのパラメータ
#define     MONITOR_PARAM_000      "000" //電圧、電流、温度、電源電圧 
#define     MONITOR_PARAM_001      "001" //充放電　電流
#define     MONITOR_PARAM_002      "002" //内部抵抗値

#define     RETURN_PARAM_OK         "OK"
#define     RETURN_PARAM_NG         "NG"


//初期通信 構造体
typedef struct
{
    // BMS向け監視PCに送信するデータの先頭
    char  FarmwearVersion[20];                  // ファームウェアバージョン
    char  ParameterVersion[20];                 // パラメータバージョン
    char  CalibratonInformation[20];                // キャリブレーション情報
    char  BoardNo[2];                       // 基板 No
    char  reserved_1[20];                       // 予備
} MEAS_DATA_INIT_st;

typedef struct
{
    union
    {
        unsigned char str[4];
        struct
        {
            float fl;
        } data;
    } fl_st;
} FLOAT_st;
/*
* システム時間
*/
typedef struct
{
    union
    {
        char  MeasureDate[3];   // 日付
        struct
        {
            uint8_t year;   //年(2)
            uint8_t month;  //月(1)
            uint8_t day;    //日(1)
        } date;
    } sysDate;

    union
    {
        char  MeaserTime[3];    // 時刻
        struct
        {
            uint8_t hour;   //時(1)
            uint8_t min;    //分(1)
            uint8_t sec;    //秒(1)
        } time;
    } sysTime;
} ST_time_t;

//モニターコマンド パラメータ１（電流、電圧、温度）
typedef struct
{
    ST_time_t Time;                             //システム時間
    int32_t   PassSecond;                           // 経過秒
//
    float I2000_measData;                       // I2000  計算結果が入る    2019.3.11 long -> float     100msec周期
    float I20_measData;                         // I20    計算結果が入る    2019.3.11 long -> float     100msec周期
    float MV_measData[M_MAX];                   // MV     計算結果が入る モジュール電圧         2019.3.11 long -> float 10msec * 8 (1ch - 15ch)
    float T_measData[2];                        // T1,T2  計算結果が入る 環境温度２CH　平均データより1秒周期で更新される。 2019.3.11 int - float
    float MT_measData[M_MAX];                   // MT     計算結果が入る モジュール温度 平均データより1秒周期で更新される。 2019.3.11 int -> float
    float SOC;                                  // 
//
    int32_t   Zan;
    int32_t   Pin;
    int32_t   Pout;
    float lave;
    float Vave;
    int32_t   Tave;
    //
    float IR;                                   // 内部抵抗
    float Vpol[2];                              // 0:1秒前　1:現在
    float Vocv[2];                              // 0:1秒前　1:現在　
    float Rah_list[2];                          // 0:1秒前　1:現在　残量量
    float Rah_cal[2];                           // 0:1秒前　1:現在　推定残量
    float Rah[2];                               // 0:1秒前　1:現在　残容量
    float Capa[2];
    float SI2000_measData;                      // SI2000 計算結果が入る    2019.3.11 long -> float
    float SI20_measData;                        // SI20   計算結果が入る    2019.3.11 long -> float
    float ACLeak_measData;                      // 計算結果が入る AC Leak  抵抗値               2019.3.11 long -> float
    float DCLeak_P_measData;                    // 計算結果が入る DC Leak + 抵抗値      20019.3.11 long -> float
    float DCLeak_N_measData;                    // 計算結果が入る DC Leak - 抵抗値          2019.3.11 long -> float
    float FANContM_PWM;
    float FANVMonM_measData;                    // FANVMon 計算結果が入る FAN電圧電源(V)    2019.3.11 long -> float
    float FANMonM_measData;                     // FANMon  計算結果が入る FAN電流検出(A)        2019.3.11 long -> float
    float Mon24V_measData;                      // 計算結果が入る   2019.3.11 long -> float
    float MonUPS_measData;                      // 計算結果が入る   2019.3.11 long -> float
//
    int32_t   BMS_PWM;
    int32_t   INV_Ready;
    int32_t   CHG_POW;
    int32_t   DCHG_POW;
    //
    char  reserved_2[2];
    char  reserved_3[2];
    char  reserved_4[2];
    //
    unsigned short error_bit[32];               // エラービット
    //
    unsigned char temp[2];                      // 【注意！！！】エラービットとfloat の間の隙間
    float IST_measData;                         // 計算結果が入る シャント抵抗温度7 2019.3.11 int -> float
    float MonP15V_measData;                     // 計算結果が入る CT電源電圧　＋    2019.3.11 long -> float
    float MonN15V_measData;                     // 計算結果が入る CT電源電圧　−    2019.3.11 long -> float
    float Mon5V_measData;                       // 計算結果が入る シャント電源電圧  2019.3.11 long -> float
    float SI_measData;                          // 計算結果が入る シャントモジュール電圧    2019.3.11 lonhg -> float
    // ---- SOC計算用 ----
//    float Vpol_list[2];                         // 0:1秒前  1:現在
//    float Vocva[2];                             // 0:1秒前　1:現在　中央OCV
//    float Vcocva[2];                            // 0:1秒前　1:現在　充電OCV
//    float Vdcocva[2];                           // 0:1秒前　1:現在  放電OCV
//    float Ioffa;                                // 電流補正値
//    float Ioff;                                 // 電流補正値
//  
//  
//  
// BMS向け監視PCに送信するデータの最後
//    int BMSforLastData; //ここが監視PCに送る最後のデータのダミー
//
} MEAS_DATA_V_I_T_st;


// 初期通信コマンド
typedef struct
{
	char cmd[3];
	char reserved[8];
} INIT_CMD_st;

// 初期通信バッファ
typedef struct
{
	INIT_CMD_st cmd;
	MEAS_DATA_INIT_st data;
	unsigned char checkSum;
} INIT_CMD_BUF_st;

// モニタ通信コマンド
typedef struct
{
	char cmd[3];
	char reserved1[2];
	char param[3];
	char reserved2[3];
} MONITOR_CMD_st;

// モニタコマンドバッファ（電流、電圧、温度）
typedef struct
{
	MONITOR_CMD_st cmd;
	MEAS_DATA_V_I_T_st data;
	unsigned char checkSum;
} MONITOR_CMD_BUF_PARAM1_st;

void startUDP(char *dest_ip, int dest_port);
void kanpUDP(ARGV_STRUCT_st argV);
int udp_receive(SOCKET sock, char* buffer, int size, struct sockaddr* sockaddr);
void socket_close(int server);
int receiveUDP(SOCKET* sock, char* rBuff);
void dispMemDump(char* adr, uint32_t size);
void helpKanpUDP(void);
int cmdMonitor(int param);
int cmdInit(void);
int cmdGetVersion(void);

#endif //__UDP_H