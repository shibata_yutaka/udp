﻿#include <stdio.h>
#include <iostream>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <WinSock2.h> //inet_ntp()

#include "main.h"
#include "UDP.h"

SOCKET sockUdpFD; // PLC socket
struct sockaddr receiveSockAddr;



char bms_ip[20] = "192.168.1.120"; //"192.168.1.121";

int bms_port  = 8510;


//char recvBuffer[MAX_BUFSIZE];
int  recvLen = 0;

int currentNo=0;

int headerWrite=TRUE;

#define HEADER_STR_SIZE 512
char VIT_header[][HEADER_STR_SIZE] =
#if false
{
    "ファームウェアバージョン", "パラメータバージョン","キャリブレーション情報", "基板番号"
};
#else
{
    "ファームウェアバージョン", "パラメータバージョン", "キャリブレーション情報", "基板番号",
        "日付", "時刻", "時間[秒]", "I2000[A]", "I20[A]", "MV1[V]", "MV2[V]",
        "MV3[V]", "MV4[V]", "MV5[V]", "MV6[V]", "MV7[V]", "MV8[V]", "MV9[V]",
        "MV10[V]", "MV11[V]", "MV12[V]", "MV13[V]", "MV14[V]", "MV15[V]",
        "T1[℃]", "T2[℃]", "MT1[℃]", "MT2[℃]", "MT3[℃]", "MT4[℃]", "MT5[℃]",
        "MT6[℃]", "MT7[℃]", "MT8[℃]", "MT9[℃]", "MT10[℃]", "MT11[℃]",
        "MT12[℃]", "MT13[℃]", "MT14[℃]", "MT15[℃]", "SOC[%]", "残量[Ah]",
        "Pin[Kw]", "Pout[Kw]", "Iave[A]", "Vave[V]", "Tave[℃]", "IR[mΩ]",
        "Vpol[V]", "Vocv[V]", "Rah_list(Ah)", "Rah_cal(Ah)", "Rah(Ah)", "Capa.(Ah)",
        "SI2000[A]", "SI20[A]", "AC_Leak[V]", "DC_Leak(+)[V]", "DC_Leak(-)[V]", "FANContM(PWM)", "FANVmon[V]",
        "FANmon[V]", "+24VMON[V]", "UPSMON[V]", "BMS_PWM", "INV_Ready", "CHG_POW",
        "DCHG_POW", "Reserved1", "Reserved2", "Reserved3",
        "エラービット1", "エラービット2", "エラービット3", "エラービット4", "エラービット5",
        "エラービット6", "エラービット7", "エラービット8", "エラービット9", "エラービット10",
        "エラービット11", "エラービット12", "エラービット13", "エラービット14", "エラービット15",
        "エラービット16", "エラービット17", "エラービット18", "エラービット19", "エラービット20",
        "エラービット21", "エラービット22", "エラービット23", "エラービット24", "エラービット25",
        "エラービット26", "エラービット27", "エラービット28", "エラービット29", "エラービット30",
        "エラービット31", "エラービット32", "Reserved4", "IST", "P15V", "N15V", "5V",
        "YMD", "HMS", "PASS"
};
#endif

MEAS_DATA_INIT_st Init;

MONITOR_CMD_BUF_PARAM1_st ivt;

int setVITdata(char* rBuff, char* str, int size);

extern int startTimerThread(void);

extern void file_write(char* str);

/*
* 監視PCもどき
*/
void kanpUDP(ARGV_STRUCT_st argV)
{
    printf("%s %d\n", __func__, __LINE__);

    char* strp = argV.strP;
    char* ctx  = argV.ctx;

    int size=0;
    int ret = 0;
    char rBuff[MAX_BUFSIZE];

    char data[256] = "";
    memset(data, NULL, sizeof(data));


    printf("%s %d strS=%s strP=%s\n", __func__, __LINE__, argV.strS,argV.strP);
    strp = strtok_s(NULL, " \n\r", &ctx);

    if (strp == NULL)
    {
        strcpy_s(bms_ip, "192.168.1.120");
        startTimerThread(); //スレッドで監視PCから1秒ごとに取得コマンドを送信する
    }
    else if (strcmp(strp, "120") == 0)
    {
        strcpy_s(bms_ip, "192.168.1.120");
        startTimerThread(); //スレッドで監視PCから1秒ごとに取得コマンドを送信する
    }
    else if (strcmp(strp, "121") == 0)
    {
        strcpy_s(bms_ip, "192.168.1.121");
        startTimerThread(); //スレッドで監視PCから1秒ごとに取得コマンドを送信する
    }
    else if (strcmp(strp, "122") == 0)
    {
        strcpy_s(bms_ip, "192.168.1.122");
        startTimerThread(); //スレッドで監視PCから1秒ごとに取得コマンドを送信する
    }
    else if (strcmp(strp, "123") == 0)
    {
        strcpy_s(bms_ip, "192.168.1.123");
        startTimerThread(); //スレッドで監視PCから1秒ごとに取得コマンドを送信する
    }
    else
    {
        printf("%s %d strS=%s strP=%s\n", __func__, __LINE__, argV.strS, argV.strP);
        // data フォルダが無い場合はフォルダを作成する /////////
        char cmd[100];
        sprintf_s(cmd, "if not exist data mkdir data");
        system(cmd);

        //通信初期化
        startUDP((char*)bms_ip, bms_port);

        if (strcmp(strp, INIT_CMD) == 0)
        {
            //初期通信要求
            cmdInit();
        }
        else if (strcmp(strp, MONITOR_CMD) == 0)
        {
            //モニタ要求
            int param = 0;
            strp = strtok_s(NULL, " \n\r", &ctx);
            if (strp == NULL)
            {
                param = 0;
            }
            else
            {
                // パラメータを数値に変換
                param = atoi(strp);
            }
            cmdMonitor(param);
        }
        else if (strcmp(strp, GET_VERSION_CMD) == 0)
        {
            //バージョン要求
            cmdGetVersion();
        }
        else if (strcmp(strp, "help") == 0)
        {
            size = 0;
            helpKanpUDP();
        }
        else
        {
            size = 0;
            helpKanpUDP();
        }

        if (size > 0)
        {
          
            int size = receiveUDP(&sockUdpFD,&rBuff[0]);
        }

    }
    
}
/*
* モニターコマンド送信
*/
int cmdMonitor(int param)
{
    int size = 0;
    int ret = 0;
    char data[256] = "";


    // MRC モニタコマンド : CMD(3),PARAM(3)
    memset(data, 0x00, sizeof(data));
    sprintf_s(data, "%s%02d%03d", MONITOR_CMD, 0, param);
    size = (int)strlen(data);

    memset(data, NULL, sizeof(data));
    // IRC 初期化 : CMD(3)
    memset(data, 0x00, sizeof(data));
    memcpy(data, MONITOR_CMD, strlen(MONITOR_CMD));
    size = (int)strlen(data);
    printf("%s %d size=%d data=%s\n\r", __func__, __LINE__, size, data);

    //RackBMSへコマンド送信
    send(sockUdpFD, (char*)&data, (int)strlen(data), 0);

    return ret;
}
/*
* 初期通信コマンド送信
*/
int cmdInit(void)
{
    int size = 0;
    int ret = 0;

    char data[256] = "";

    // IRC 初期化 : CMD(3)
    memset(data, 0x00, sizeof(data));
    memcpy(data, INIT_CMD, strlen(INIT_CMD));
    size = (int)strlen(data);
    printf("%s %d size=%d data=%s\n\r", __func__, __LINE__, size, data);


    //RackBMSへコマンド送信
    send(sockUdpFD, (char*)&data, (int)strlen(data), 0);

    return ret;
}

/*
* バージョン取得コマンド
*/
int cmdGetVersion(void)
{
    int size = 0;
    int ret = 0;

    char data[256] = "";

    // GVR バージョンの取得 : CMD(3)
    memset(data, 0x00, sizeof(data));
    memcpy(data, GET_VERSION_CMD, strlen(GET_VERSION_CMD));
    size = (int)strlen(data);
    printf("%s %d size=%d data=%s\n\r", __func__, __LINE__, size, data);


    //RackBMSへコマンド送信
    send(sockUdpFD, (char*)&data, (int)strlen(data), 0);

    return ret;
}

/*
* ヘルプ表示
*/
void helpKanpUDP(void)
{
    printf("kanp\n");
    printf("GVR : Get Version\n");
    printf("IRC : INIT\n");
    printf("MRC <param> : Monitor <param=0,1,2...>\n");
}

/*
*  UDP通信開始処理
* 引数(1) char *dist_ip  : 送信先のIP アドレス
* 引数(2) int  dist_port : 送信先のポート
*/
void startUDP(char *dist_ip, int dist_port)
{
    printf("%s %d IP=%s Port=%d\n", __func__, __LINE__,dist_ip, dist_port);

    struct sockaddr_in bmsAddr;

    // ソケット生成
    if ((sockUdpFD = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
    {
        printf("%s %d client socket make error\n", __func__, __LINE__);
        perror("socket");
    }
    else
    {
        printf("%s %d sockUdpFD=%d\n",__func__,__LINE__,(int)sockUdpFD);
    }


    // 送信先アドレス・ポート番号設定
    // *dist_ip  : 送信先のIP アドレス
    // dist_port : 送信先のポート
    bmsAddr.sin_family = AF_INET;
    inet_pton(AF_INET, dist_ip, &bmsAddr.sin_addr.s_addr);
    bmsAddr.sin_port = htons(dist_port);

    printf("%s %d Destination IP=%d.%d.%d.%d port=%d\n", __func__, __LINE__, (bmsAddr.sin_addr.s_addr & 0xff),
        ((bmsAddr.sin_addr.s_addr & 0xff00) >> 8),
        ((bmsAddr.sin_addr.s_addr & 0xff0000) >> 16),
        ((bmsAddr.sin_addr.s_addr & 0xff000000) >> 24),
        ntohs(bmsAddr.sin_port)
    );


    if (connect(sockUdpFD, (struct sockaddr*)&bmsAddr, sizeof(bmsAddr)) < 0)
    {
        printf("%s %d sockUdpFD=%d\n", __func__, __LINE__, (int)sockUdpFD);
    }
    else
    {
        printf("%s %d sockUdpFD=%d\n", __func__, __LINE__, (int)sockUdpFD);
    }
}

/*
* 関数名  : receiveUDP
* 処理    : UDP受信処理
* 引数(1) : SOCKET *sock 受信ソケットアドレス
* 引数(2) : char* rBuff 受信バッファ
* 戻り値  : int size 受信データ数
* 
*/
int receiveUDP(SOCKET* sock, char* rBuff)
{
    int size = -1;

    printf("%s %d sock=%d\n", __func__, __LINE__,(int)sockUdpFD);

    while (1)
    {
        //memset(&recvBuffer, 0x00, sizeof(recvBuffer));
        memset(rBuff, 0x00, sizeof(rBuff)); //受信バッファのクリア
        //size = udp_receive(sockUdpFD, &recvBuffer[0], MAX_BUFSIZE, &receiveSockAddr);
        size = udp_receive(sockUdpFD, rBuff, MAX_BUFSIZE, &receiveSockAddr);

        if (size < 0) {
            perror("recvfrom() failed.");
            //return MSG_FAILURE;
            return size;
        }
        else
        {
            // メモリーダンプ
            //dispMemDump(rBuff, size);
            return size;
        }
    }
    return size;
}


/*
*  UDP 受信処理
*/
int udp_receive(SOCKET sock, char* buffer, int size, struct sockaddr* sockaddr)
{
    int sockaddrLen = sizeof(*sockaddr);
    int receivedSize = recvfrom(sock, buffer, MAX_BUFSIZE, 0, sockaddr, &sockaddrLen);
    if (receivedSize < 0) {
        perror("recvfrom() failed.");
        //return MSG_FAILURE;
        return 0;
    }

    //<Winsock2.h>
    int port = ntohs(((struct sockaddr_in*)sockaddr)->sin_port);    
    char str[INET_ADDRSTRLEN];
    inet_ntop(((struct sockaddr_in*)sockaddr)->sin_family, &((struct sockaddr_in*)sockaddr)->sin_addr,str,sizeof(str));
    printf("%s %d Receive IP=%s Port=%d\n",__func__,__LINE__,str,port);
    
    return receivedSize;
}
/*
* ソケットのクローズ
*/
void socket_close(int server)
{
    if (closesocket(server) < 0) {
        perror("close() failed.");
        //exit(EXIT_FAILURE);
    }
}

/*
* メモリーダンプ
*/
void dispMemDump(char *adr, uint32_t size)
{
    printf("%s %d size=%d\n", __func__, __LINE__, size);
    //for (int i = 0; i < (int)size; i++)
    //{
        //printf("%02X ",(*(adr+i)&0xFF));
    //}
    //printf("\n");


    printf("     ");
    for (int j = 0; j < 16; j++)
    {
        printf("%02X ", j);
    }
    printf("\n");
    for (int j = 0; j < (int)size; j++)
    {
        if ((j % 16) == 0)
        {
            printf("%04X ", j);
        }
        printf("%02X ", ( *(adr+j) & 0xFF));
        if ((j % 16) == 15)
        {
            printf("\n");
        }
    }
    printf("\n");
}

/*
* チェックサムの計算
* 引数(1) char * rBuff アドレス
* 引数(2) int  num サイズ
* 戻り値 int ret (TRUE,FALSE)
*/
int culcCheckSum(char* rBuff,int num)
{
    int ret = FALSE;

    int i = 0;
    unsigned char checkSumTotal = 0;
    unsigned char checkSum = 0;
    unsigned char checkSumCulc = 0;
    
    for (i = 0; i < num; i++)
    {
        checkSumTotal += (unsigned char)*(rBuff + i);
    }
    checkSum = (unsigned char)*(rBuff + i);
    checkSumCulc = (unsigned char)~checkSumTotal;
    if (checkSum == checkSumCulc)
    {
        printf("%s %d OK checkSum=%02X checkSumCulc=%02X\n", __func__, __LINE__, checkSum, checkSumCulc);
        ret = TRUE;
    }
    else
    {
        printf("%s %d NG checkSum=%02X checkSumCulc=%02X\n", __func__, __LINE__, checkSum, checkSumCulc);
        ret = FALSE;
    }
    return ret;
}
/*
* 関数 analyzeReciveCmd
* 説明 受信したコマンドを解析する
* 引数(1) : char* rBuff 受信バッファ
* 引数(2) : char* str 受診結果出力バッファ
* 戻り値  : int ret 0<=ファイル出力あり -1=フィアル出力無し
*/
int analyzeReciveCmd(char* rBuff, char* str,int size)
{
    int ret = -1;
    int retCheckSum = 0;

    time_t now;
    struct tm local;
    errno_t err;

    now = time(NULL); //現在時刻の取得
    err = localtime_s(&local, &now);

    char cmd[5] = { NULL };
    strncpy_s(cmd, &rBuff[0], 3);
    if (strncmp(cmd, INIT_CMD,sizeof(INIT_CMD)) == 0)
    {
        INIT_CMD_BUF_st* init;
        init = (INIT_CMD_BUF_st *)rBuff;

        // グローバル変数にコピーしておく
        memcpy(&Init, (char *)&init->data, sizeof(Init));
        printf("%s %d CMD=%s\n", __func__, __LINE__, INIT_CMD);
        ret = -1; //ファイルへは書き出さない
    }
    else if (strncmp(cmd, MONITOR_CMD,sizeof(MONITOR_CMD)) == 0)
    {
        MONITOR_CMD_st* mon;
        mon = (MONITOR_CMD_st*)rBuff;

        if (strncmp(mon->param, MONITOR_PARAM_000, sizeof(MONITOR_PARAM_000)) == 0)
        {
            retCheckSum = culcCheckSum(rBuff + MONITORING_PC_COMM_SEND_CMD_LENGTH, MONITORING_PC_MONITOR_DATA_NUM);
            if (retCheckSum == TRUE)
            {
                //電流、電圧、温度
                ret = setVITdata(rBuff, str, size);
            }
        }
        else
        {
            printf("%s %d CMD=%s PARAM=%s\n",__func__,__LINE__, MONITOR_CMD, mon->param);
            retCheckSum = culcCheckSum(rBuff + MONITORING_PC_COMM_SEND_CMD_LENGTH, MONITORING_PC_MONITOR_DATA_NUM);
            if (retCheckSum == TRUE)
            {
                //電流、電圧、温度
                ret = setVITdata(rBuff, str, size);
            }
        }
    }
    else
    {
        ret = -1;
    }

    return ret;
}

/*
* 
* 関数    : setVITdata
* 説明    : 電流、電圧、温度 をバッファにセットする
* 引数(1) : char* rBuff 受信バッファ
* 引数(2) : char* str 受診結果出力バッファ
* 戻り値  : int ret 0<=ファイル出力あり -1=フィアル出力無し
*/
int setVITdata(char* rBuff, char* str, int size)
{
    int ret = -1;

    time_t now;
    struct tm local;
    errno_t err;

    now = time(NULL); //現在時刻の取得
    err = localtime_s(&local, &now);

    //char tmp[1024];

    //ヘッダーの書き出し
    if(headerWrite==TRUE)
    {
        headerWrite=FALSE;
        int len = sizeof(VIT_header) / HEADER_STR_SIZE;
        for (int i = 0; i < len; i++)
        {
            sprintf_s(str, size, "%s,", (char*)&VIT_header[i]);
            file_write(str);
        }
        sprintf_s(str, size, "\n");
        file_write(str);
    }
    
    //電流、電圧、温度

    //MONITOR_CMD_st* cmd = (MONITOR_CMD_st*)rBuff;
    //MEAS_DATA_V_I_T_st* data = (MEAS_DATA_V_I_T_st*)(rBuff+ MONITORING_PC_COMM_SEND_CMD_LENGTH);
    memset(&ivt, NULL, sizeof(ivt));
    memcpy(&ivt.cmd, rBuff,sizeof(MONITOR_CMD_st));

    int num = sizeof(MONITOR_CMD_st);
    memcpy(&ivt.data.Time, rBuff + num ,sizeof(ST_time_t));
    num += sizeof(ST_time_t);
    memcpy(&ivt.data.PassSecond, rBuff + num, sizeof(MEAS_DATA_V_I_T_st) +1);
    printf("%s %d size=%d Pass=%d I2000=%lf I20=%lf CheckSum=%02X\n", __func__, __LINE__,
        (int)sizeof(MONITOR_CMD_BUF_PARAM1_st),
        ivt.data.PassSecond, //経過時間[秒] 0,
        ivt.data.I2000_measData / 1000, //I2000[A] 0.0,
        ivt.data.I20_measData / 1000,    //I20[A]
        (int)ivt.checkSum
    );


    //printf("%s %d %s %s %d\n",__func__,__LINE__,
    //    ivt.cmd.cmd,
    //    ivt.cmd.param,
    //    ivt.data.PassSecond
    //    );

    sprintf_s(str, size, "%s,%s,%s,%d,%04d/%02d/%02d,%02d:%02d:%2d,%d,%lf,%lf",
        Init.FarmwearVersion,
        Init.ParameterVersion,
        Init.CalibratonInformation,
        atoi(Init.BoardNo), //基板番号
        //         
        local.tm_year + 1900,//日付 2020/11/05,
        local.tm_mon + 1,
        local.tm_mday,
        local.tm_hour,//時刻 13:53:03,
        local.tm_min,
        local.tm_sec,
        //
        ivt.data.PassSecond, //経過時間[秒] 0,
        ivt.data.I2000_measData / 1000, //I2000[A] 0.0,
        ivt.data.I20_measData / 1000    //I20[A]
        //I20[A] 0.0,
        //MV1[V]...MV15[V] 0.0,
        //T1[℃],T2[℃], 0.0,
        //MT1[℃]...MT15[℃] 0.0,
        //SOC[%], 0,
        //残量[Ah], 0,
        //Pin[Kw], 0
        //
    );
    file_write(str);

    // MV1-15
    for (int i=0; i < 15; i++)
    {
        sprintf_s(str, size, ",%lf", ivt.data.MV_measData[i]/1000);
        //strcat_s(str, size, tmp);
        file_write(str);
    }

    // T1,T2
    sprintf_s(str, size, ",%lf,%lf", ivt.data.T_measData[0]/10, ivt.data.T_measData[1]/10);
    //strcat_s(str, size, tmp);
    file_write(str);

    // MT1-15
    for (int i = 0; i < 15; i++)
    {
        sprintf_s(str, size, ",%lf", ivt.data.MT_measData[i]/10);
        //strcat_s(str, size, tmp);
        file_write(str);
    }

    // soc,zan,Pin,Pout,lave,Vave,Tave,IR
    //                     1   2  3  4  5   6   7  8   9   10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25 26 27 28 29 30 31
    sprintf_s(str, size, ",%lf,%d,%d,%d,%lf,%lf,%d,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%d,%d,%d,%d,%d,%d,%d"
        //1             2             3             4              5              6              7
        , ivt.data.SOC, ivt.data.Zan, ivt.data.Pin, ivt.data.Pout, ivt.data.lave/1000, ivt.data.Vave/1000, ivt.data.Tave/10 //1-7
        //8            9                 10                 11                    12
        //, ivt.data.IR, ivt.data.Vpol[0]/1000, ivt.data.Vocva[0]/1000, ivt.data.Rah_list[0]/1000, ivt.data.Rah_cal[0]/1000 //8-12
        ,0.0,0.0,0.0,0.0,0.0
        //13               14                15                        16
        , ivt.data.Rah[0]/1000, ivt.data.Capa[0]/1000, ivt.data.SI2000_measData/1000, ivt.data.SI20_measData/1000 //13-16
        //17                        18                          19  
        , ivt.data.ACLeak_measData/1000, ivt.data.DCLeak_P_measData/1000, ivt.data.DCLeak_N_measData/1000
        //20                     21                          22
        , ivt.data.FANContM_PWM, ivt.data.FANVMonM_measData/1000, ivt.data.FANMonM_measData/1000
        //23                        24                      
        , ivt.data.Mon24V_measData/1000, ivt.data.MonUPS_measData/1000
        //25                26                  27                28
        , ivt.data.BMS_PWM, ivt.data.INV_Ready, ivt.data.CHG_POW, ivt.data.DCHG_POW
        //29                   30                   31
        , ivt.data.reserved_2[0], ivt.data.reserved_3[0], ivt.data.reserved_4[0]
    );
    //strcat_s(str, size, tmp);
    file_write(str);
    
    // error_bit
    for (int i = 0; i < 32; i++)
    {
        sprintf_s(str, size, ",%d", ivt.data.error_bit[i]);
        //strcat_s(str, size ,tmp);
        file_write(str);
    }
    // IST,+15V,-15V,5V
    sprintf_s(str, size, ",0,%lf,%lf,%lf,%lf"
        , ivt.data.IST_measData/10, ivt.data.MonP15V_measData/1000, ivt.data.MonN15V_measData/1000, ivt.data.Mon5V_measData/1000
    );

    file_write(str);

    sprintf_s(str, size, ",%d/%d/%d,%02d:%02d:%02d,%d\n"
        , ivt.data.Time.sysDate.date.year
        , ivt.data.Time.sysDate.date.month
        , ivt.data.Time.sysDate.date.day
        , ivt.data.Time.sysTime.time.hour
        , ivt.data.Time.sysTime.time.min
        , ivt.data.Time.sysTime.time.sec
        , ivt.data.PassSecond
    );

    printf("%s %d CMD=%s PARAM=%s str=%s\n", __func__, __LINE__, MONITOR_CMD, MONITOR_PARAM_000, str);
    ret = (int)strlen(str); //ファイルに書き出すサイズ            

    return ret;
}