
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <iostream>

#include "main.h"


typedef struct
{
    FILE* fp;
    int state;
    int seq_no;
    int diff_time[2];
    time_t start_time;
    char fileName[256];
} FILE_st;

FILE_st file =
{
    NULL,       //fp
    FILE_OPEN,  //state
    1,          //seq_no
    {0,-1},     //diff_time[2]
    time(NULL),  //start_time;
    "IVT"          //FileName
};

//FILE* fp;
//int file_state = FILE_OPEN;
//int file_seq_no = 1;
//int diff_time[2] = { 0,-1 };

FILE* file_open(int seq_no, char* fileName);
void file_write(char* str);

extern char bms_ip[];
/*
* 関数名 : file_write
*
* 引数(1) : char* str : ファイル出力する文字列
* 戻り値  : void 無し
* 説明    : 引数の文字列をファイルに書き出す
*/
void file_write(char* str)
{
/*
* ファイル関係 開始
*/

    file.diff_time[0] = (int)difftime(time(NULL), file.start_time);

    if (file.diff_time[0]==0)
    {
        // 60秒*60分=1時間経過で 余り 0 になる
    }

    else if ((file.diff_time[0] % (60 * 60)) == 0)
    {
        //printf("%s %d file_state=%d file.diff_time[0]=%d [1]=%d\n",__func__,__LINE__,file.state,file.diff_time[0],file.diff_time[1]);
        if (file.diff_time[0] == file.diff_time[1])
        {
        }
        else
        {
            file.diff_time[1] = file.diff_time[0];
            if (file.state == FILE_WRITE)
            {
                file.state = FILE_CLOSE;
            }
        }

    }
    if (file.state == FILE_CLOSE)
    {
        fclose(file.fp);
        file.state = FILE_OPEN;
    }
    if (file.state == FILE_OPEN)
    {
        file.fp = file_open(file.seq_no,(char*)&file.fileName);
        if (file.fp == NULL)
        {
            return;
        }
        file.seq_no++;
        file.state = FILE_WRITE;
    }

/*
* ファイル関係 書き出し
*/
    //printf("%s %d str=%s", __func__, __LINE__, str);
    fprintf(file.fp, "%s", str);
    fflush(file.fp);

}
/*
* 関数名:file_open
* 引数(1) : int seq_no シーケンス番号
* 引数(2) : char* ファイル名
* 戻り値  : FILE* fp ファイル番号
*/
FILE* file_open(int seq_no,char* fileName)
{
	char file_name[256];
	time_t now;
	struct tm local;
	errno_t err;

    // data フォルダが無い場合はフォルダを作成する /////////
    char cmd[100];
    char dir[100];
    char tmp[100];
    int ipNo = 120;

    strcpy_s(tmp, bms_ip + 10);
    ipNo = atoi(tmp);

    //Log フォルダが無い場合はフォルダを作成する
    sprintf_s(cmd, "if not exist Log mkdir Log");
    system(cmd);  //#include <iostream>

    sprintf_s(dir, "Log\\Log%d", ipNo);
    printf("%s %d dir=%s\n", __func__, __LINE__, dir);
    sprintf_s(cmd, "if not exist %s mkdir %s", dir, dir);
    system(cmd);  //#include <iostream>

	now = time(NULL); //現在時刻の取得
	err = localtime_s(&local,&now);
	sprintf_s(file_name, "%s\\%s_%04d%02d%02d_%02d%02d%02d_%06d.csv",dir, fileName
		, local.tm_year + 1900
		, local.tm_mon + 1
		, local.tm_mday
		, local.tm_hour
		, local.tm_min
		, local.tm_sec
		, seq_no
	);



	err = fopen_s(&file.fp, file_name, "w");
	if(err!=0)
	{
		printf("%s %d file open error %s\n", __func__, __LINE__, file_name);
		return NULL;
	}
	else
	{
		printf("%s %d file open OK %s\n", __func__, __LINE__, file_name);
	}
	return file.fp;
}

